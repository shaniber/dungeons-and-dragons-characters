# Garim Gamwort
    Chaotic Good Male Lightfoot Halfling
    Level 2 Rogue

## Combat
    Summary
    ------
        Hit Die:        3d8 ...
        Initiative:     +4
        Speed:          25
        BAB:            +0
        Inspiration:    0
        Proficiency:    +2
        Psv Perception: 14
    Defense
    -------
        Armour Class:
            Total | Touch | Flat Footed
              15  |  14   |  11
        Hit Points
            Total:      26
            ..... ..... ..... .....
            ..... .
            Temp:        0
            -
        Death Saves:
            success: ...
            failure: ...
        Conditions:
            - 
        Effects:
            -
        Saving Throws
            Str:        -1
          * Dex:        +4
            Con:        +3
          * Int:        +4
            Wis:         0
            Cha:        +2
    Offense
    -------
        Base Attack
            Melee:       0
            Ranged:      0
            Grapple:    -1
        Rapier:
            to hit:     +6
            damage:     1d8+4
            critical:   20/x0
            reach:      5
        Dagger:
            to hit:     +6
            damage:     1d4+4
            critical:   20/x0
            reach:      5
            range:      20/60
        Shortbow:
            to hit:     +6
            damage:     1d6+4
            critical:   20/x0
            range:      30/120

## Stats
    Abilities
    --------
             Current     Base   Race  Misc
             Score  Mod  Score  Adj   Adj
        Str:   8     -1    8    --    --
        Dex:  18     +4   16    +2    --
        Con:  16     +3   16    --    --
        Int:  13     +2   13    --    --
        Wis:  10      0   10    --    --
        Cha:  14     +2   13    +1    --
    Skills
    ------
        Skill                   Modifier
        Acrobatics (Dex)          +4
        Animal Handling (Wis)      0
        Arcana (Int)              +1
        Athletics (Str)           -1
      * Deception (Cha)           +4
        History (Int)             +1
      * Insight (Wis)             +2
      * Intimidation (Cha)        +4
        Investigation (Int)       +1
        Medicine (Wis)             0
        Nature (Int)              +1
      * Perception (Wis)          +4
        Performance (Cha)         +2
      * Persuasion (Cha)          +4
        Religion (Int)            +1
        Sleight of Hand (Dex)     +4
      * Stealth (Dex)             +6
        Survival (Wis)             0
    Proficiencies
    -------------
        Weapons
            Club, Crossbow (Hand), Crossbow (Light), Dagger, Dart, Greatclub, 
            Handaxe, Javelin, Light Hammer, Longsword, Mace, Quarterstaff, Rapier, 
            Shortbow, Shortsword, Sickle, Sling, Spear, Unarmed Strike
        Tools
            Thieves' Tools, Dragonchess
    Special
    -------
        Brave
            You have advantage on saving throws against being frightened. 
        Halfling Nimbleness
            You can move through the space of any creature that is of a size 
            larger than yours. 
        Lucky
            When you roll a 1 on an attack roll, ability check, or saving 
            throw, you can reroll the die and must use the new roll. 
        Naturally Stealthy
            You can attempt to hide even when you are obscured only by a 
            creature that is at least one size larger than you.
        Expertise
            At 1st level, choose two of your skill proficiencies, or one of 
            your skill proficiencies and your proficiency with thieves' tools.
            Your proficiency bonus is doubled for any ability check you make 
            that uses either of the chosen proficiencies. At 6th level, you 
            can choose two more of your proficiencies (in skills or with 
            thieves' tools) to gain this benefit. 
        Sneak Attack
            Beginning at 1st level, you know how to strike subtly and exploit 
            a foe's distraction. Once per turn, you can deal an extra 1d6 
            damage to one creature you hit with an attack if you have 
            advantage on the attack roll. The attack must use a finesse or a 
            ranged weapon. You don't need advantage on the attack roll if 
            another enemy of the target is within 5 feet of it, that enemy 
            isn't incapacitated, and you don't have disadvantage on the attack 
            roll. 
        Cunning Action
            Starting at 2nd level, your quick thinking and agility allow you 
            to move and act quickly. You can take a bonus action on each of 
            your turns in combat. This action can be used only to take the 
            Dash, Disengage, or Hide action.
        Roguish Archetype
            Arcane Trickster. See Magic section below for spell list.
        Mage Hand Legerdemain
            Starting at 3rd level, when you cast mage hand, you can make the 
            spectral hand invisible, and you can perform the following 
            additional tasks with it:
            • You can stow one object the hand is holding in a container worn 
              or carried by another creature.
            • You can retrieve an object in a container worn or carried by 
              another creature.
            • You can use thieves’ tools to pick locks and disarm traps at 
              range.
            You can perform one of these actions without being noticed by a 
            creature if you succeed on a Dexterity (Sleight of Hand) check 
            contested by the creature’s Wisdom (Perception) check. In 
            addition, you can use the bonus action granted by your Cunning 
            Action to control the hand.
    Misc
    ----

## MAGIC
    Special
    -------
    Rogue
    ------
        DC:         9
        Attack:     3
        Level:      0     1     2     3     4
        Known:      3     3     -     -     -
        Per Day:    i     2     -     -     -
        Level 1
            . : Sleep
            . : Sleep

## Equipment
    Funds
    ----
        PP:
        GP:   230
        EP:     5 
        SP:   311
        CP:
    Inventory
    --------
        Item                          Location    Quantity    Weight
         Leather Armour                Worn
         Rapier                        Scabbard    1
         Shortbow                      Back        2
         Quiver (19 Arrows)            Back        1
         Daggers                       Boots       2
         Essence of Nightshade
         Essence of Mercury
         Essence of Dragon's Bile
         Explorer's Pack
         Thieves Tools
         Dragonchess Set
         Crowbar
         Dark, Hooded clothing
         moneypouch
         Horse and cart
         wolf hide, teeth and claws
         ornate chest housing emerald necklace
         jade frog w/ gold eyes (40gp)
         ring (50gp)
    Details
    -------

## Description
    Name:         Garim Gamwort
    Race:         Halfling
    Classes:      Rogue 3
    XP:           1772
    Alignment:    Chaotic Good
    Vision:       Low-light
    Speed:        25ft.
    Languages:    Common, Halfling
    Age:          42        Gender:       male      
    Height:       3'4"      Weight:       38lbs.
    Skin Tone:    tan       Eyes:         hazel     
    Handedness:   right     Hair:         wavy brown
    Quirks:       Garim is always looking around, like he is certain something 
                  is going to jump out at him. Not nervous, per se, just very
                  cautious.
    Speech Style: Garim speaks easily, but thoughtfully. Does not run his
                  mouth quickly, but will always have a reassuring word,
                  especially when things are about to go off the rails. 
    Quotes:       "Trust me!" is one of his favourite phrases, usually said
                  with a quick wink, right before the plan goes off the rails.
    Traits:       Personality  The first thing I do in a new place is note 
                               the locations of everything valuable - or where 
                               such things could be hidden.
                               I always have a plan for what to do when things 
                               go wrong.
                  Ideal        People. I'm loyal to my friends and family, not 
                               to any ideals, and everyone else can take a 
                               trip down the Styx for all I care. (Neutral)
                  Bond         My ill-gotten gains go to support my family.
                  Flaw         If there's a plan, I'll forget it. If I don't 
                               forget it, I'll ignore it.
    Background:     Garim has been on the road around the countryside, bringing
                  things to people that should not necessarily be brought to
                  them. A smuggler for most of his life, he chose this path
                  in order to support his sick mum back in Ascore. Every
                  once in a while he will pack up and travel back to deliver 
                  some money, and make sure she is alright.
                    He has found himself in Neverwinter, after hearing of an 
                  opportunity for a big score. He figures that if he can make 
                  one more big job, he will be able to head home and give mum 
                  the rest she truly deserves before she dies.
                    He wears, currently, a heavy leather overcoat that 
                  functions as leather armour, sporting a bright purple shirt
                  beneath. He brandishes his rapier on his belt, and when 
                  traveling, straps his short-bow to his back. He keeps a 
                  neatly trimmed and sculpted beard, and his wavy, brown
                  hair falls nearly to his shoulders.
                    Bestowed the honour of "Cloak Garim Gamwort", member of the
                  Lord's Alliance.
