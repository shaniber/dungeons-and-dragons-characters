# Garim Gamwort 
    Chaotic Good Male Lightfoot Halfling
    Level 5 Rogue

## Stats

### Abilities
         Current     Base   Race  Misc
         Score  Mod  Score  Adj   Adj
         ------------------------------
    Str:   8     -1    8    --    --
    Dex:  19     +4   16    +2    --
    Con:  16     +3   16    --    --
    Int:  13     +1   13    --    --
    Wis:  10      0   10    --    --
    Cha:  14     +2   13    +1    --

### Skills
    Skill                   Modifier
    -----                   --------
    Acrobatics (Dex)          +7 *
    Animal Handling (Wis)      0
    Arcana (Int)              +1
    Athletics (Str)           -1
    Deception (Cha)           +5 *
    History (Int)             +1
    Insight (Wis)             +3 *
    Intimidation (Cha)        +2 
    Investigation (Int)       +4 *
    Medicine (Wis)             0
    Nature (Int)              +4 *
    Perception (Wis)          +5 *
    Performance (Cha)         +2
    Persuasion (Cha)          +2 
    Religion (Int)            +1
    Sleight of Hand (Dex)     +4
    Stealth (Dex)             +7 *
    Survival (Wis)            +3 *
    ----
    Thieves Tools             +7 **
    Dragonchess               +3 *
     

### Proficiencies
    Armour
        Light armour
    Weapons
        Club, Crossbow (Hand), Crossbow (Light), Dagger, Dart, Greatclub, 
        Handaxe, Javelin, Light Hammer, Longsword, Mace, Quarterstaff, Rapier, 
        Shortbow, Shortsword, Sickle, Sling, Spear, Unarmed Strike
    Tools
        Thieves' Tools, Dragonchess

#### Special
    Brave
        You have advantage on saving throws against being frightened. 
    Cunning Action
        Starting at 2nd level, your quick thinking and agility allow you 
        to move and act quickly. You can take a bonus action on each of 
        your turns in combat. This action can be used only to take the 
        Dash, Disengage, or Hide action.
    Expertise
        At 1st level, choose two of your skill proficiencies, or one of 
        your skill proficiencies and your proficiency with thieves' tools.
        Your proficiency bonus is doubled for any ability check you make 
        that uses either of the chosen proficiencies. At 6th level, you 
        can choose two more of your proficiencies (in skills or with 
        thieves' tools) to gain this benefit. 
    Fast Hands
        Starting at 3rd level, you can use the bonus action granted by your 
        Cunning Action to make a Dexterity (Sleight of Hand) check, use your 
        thieves' tools to disarm a trap or open a lock, or take the Use an 
        Object action. 
    Halfling Nimbleness
        You can move through the space of any creature that is of a size 
        larger than yours. 
    Lucky
        When you roll a 1 on an attack roll, ability check, or saving 
        throw, you can reroll the die and must use the new roll. 
    Naturally Stealthy
        You can attempt to hide even when you are obscured only by a 
        creature that is at least one size larger than you.
    Roguish Archetype
        Scout.
    Skirmisher
        Starting at 3rd level, you are difficult to pin down during a fight. 
        You can move up to half your speed as a reaction when an enemy ends
        its turn within 5 feet of you. This movement doesn't provoke 
        opportunity attacks.
    Sneak Attack
        Beginning at 1st level, you know how to strike subtly and exploit 
        a foe's distraction. Once per turn, you can deal an extra 1d6 
        damage to one creature you hit with an attack if you have 
        advantage on the attack roll. The attack must use a finesse or a 
        ranged weapon. You don't need advantage on the attack roll if 
        another enemy of the target is within 5 feet of it, that enemy 
        isn't incapacitated, and you don't have disadvantage on the attack 
        roll. 
    Survivalist
        When you choose this archetype at third level, you gain proficiency
        in the Nature and Survival skills. Your proficiency bonus is doubled
        for any ability check you make that uses either of those proficiencies.
    Uncanny Dodge
        Starting at 5th level, when an attacker that you can see hits you with
        an attack, you can use your reaction to halve the attack's damage 
        against you.
    
#### Feats
    Burglar 
        You pride yourself on your quickness and your close study of certain 
        clandestine activities. You gain the following benefits: 
            - Increase your Dexterity score by 1, to a maximum of 20. 
            - You gain proficiency with thieves' tools. If you are already 
              proficient with them, you add double your proficiency bonus to 
              checks you make with them. 

#### Misc
    None.
