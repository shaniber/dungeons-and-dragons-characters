# Garim Gamwort
    Chaotic Good Male Lightfoot Halfling
    Level 5 Rogue

## Combat

### Summary
    Hit Die:        5d8 .....
    Initiative:     +4
    Speed:          25
    BAB:            +0
    Proficiency:    +2
    Psv Perception: 14
    Inspiration:    0

### Defense
    Armour Class:
        Total | Touch | Flat Footed
          15  |  14   |  11
    Hit Points
        Total:      41
        xxxxx xxxxx xxxxx .....    
        ..... ..... ..... .....
        .
        
        Temp:        0
        -
    Death Saves:
        success: ...
        failure: ...
    Conditions:
        - 
    Effects:
        - 
    Saving Throws
        Str:        -1
        Dex:        +6 *
        Con:        +3
        Int:        +3 *
        Wis:         0
        Cha:        +2

### Offense
    Base Attack
        Melee:       0
        Ranged:      0
        Grapple:    -1
    Rapier:
        to hit:     +7
        damage:     1d8+4
        critical:   20/x2
        reach:      5
    Dagger:
        to hit:     +7
        damage:     1d4+4
        critical:   20/x2
        reach:      5
        range:      20/60
    Shortbow:
        to hit:     +7
        damage:     1d6+4
        critical:   20/x2
        range:      80/320
