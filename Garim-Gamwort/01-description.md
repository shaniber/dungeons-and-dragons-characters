# Garim Gamwort

    Chaotic Good Male Lightfoot Halfling
    Level 5 Rogue

## Description

    Name:         Garim Gamwort
    Race:         Halfling
    Classes:      Rogue 5
    XP:           7325
    Alignment:    Chaotic Good
    Vision:       Low-light
    Speed:        25ft.
    Languages:    Common, Halfling
    Age:          42        Gender:       male      
    Height:       3'4"      Weight:       38lbs.
    Skin Tone:    tan       Eyes:         hazel     
    Handedness:   right     Hair:         wavy brown
    Quirks:       Garim is always looking around, like he is certain something 
                  is going to jump out at him. Not nervous, per se, just very
                  cautious.
    Speech Style: Garim speaks easily, but thoughtfully. Does not run his
                  mouth quickly, but will always have a reassuring word,
                  especially when things are about to go off the rails. 
    Quotes:       "Trust me!" is one of his favourite phrases, usually said
                  with a quick wink, right before the plan goes off the rails.
    Traits:       Personality  The first thing I do in a new place is note 
                               the locations of everything valuable - or where 
                               such things could be hidden.
                               I always have a plan for what to do when things 
                               go wrong.
                  Ideal        People. I'm loyal to my friends and family, not 
                               to any ideals, and everyone else can take a 
                               trip down the Styx for all I care. (Neutral)
                  Bond         My ill-gotten gains go to support my family.
                  Flaw         If there's a plan, I'll forget it. If I don't 
                               forget it, I'll ignore it.
    Background:     I was raised in Amnwater by my mother, Valyse Gamwort (nee 
                  Hatrix). My father, Gorim Gamwort, was a member of the city 
                  militia, and was killed when attempting to arrest a 
                  murderer. She escaped, having taken three other militia 
                  constables out during the arrest attempt.
                    I had a happy childhood, aside from losing my father a 
                  young age, with many extended family members around helping 
                  to raise me. My father's mother, who I called Gamgee, would 
                  feed me butterscotch candies when I helped her in her 
                  garden. My aunt on my father's side, Grayra Gamwort, helped 
                  my mother bring me up, and kept my father's memory alive 
                  with me through stories.
                    The only family that I have regular contact with is my mum, 
                  and my Aunt Grayra. I try to send letters and care packages 
                  to them regularly, and will pack up and return to Amnwater, 
                  sometimes with little warning. Grayra lives with mum, caring 
                  for her in her illness.
                    As older family members passed, the task of providing for 
                  the family fell to me. To do this, I began to work as a 
                  courier. Initially, I was a legitimate businessman, taking 
                  packages and parcels to various places for various people. 
                  Soon, though, I found that taking jobs from less mainstream 
                  sources lead to more money, which meant I was able to keep 
                  my mum more comfortable. She didn't ask questions, but 
                  Grayra is suspicious, and questions me regularly, reminding 
                  me that my father was a member of the constabulary, without 
                  saying outright that I'm dishonouring his memory.
                    I've been married twice. The first time, in my youth, was 
                  to Bregwen Odiwyn, also from Amnwater. That marriage lasted 
                  about 2 years... she didn't like that I was on the road 
                  constantly, so she left me. We remain friendly, and she 
                  checks in on mum regularly. She's a blessed soul that 
                  deserved better. I have a tattoo with her initials on my 
                  left shoulder.
                    My second marriage happened ten years later. Idazana 
                  Trynlyse was her name. She was a fellow smuggler, and we got 
                  along famously. We worked many routes and scams together... 
                  until she decided that the stakes on one job were much better 
                  suited to her alone. She stole away in the middle of the 
                  night, taking the money and my heart, leaving nothing but a 
                  note, sealed with a kiss, saying that she loved me but the 
                  gold was too good. I haven't seen her since. The pain of her 
                  betrayal has faded over the past twenty years, with only  
                  a small resentment remaining.
                    Right now, I am just trying to keep my mum comfortable. 
                  I'll send my cut of the profits back to Amnwater via a 
                  trusted courier from my own network. She's very sick though, 
                  and no doctor or sage knows why. She's old, but if she were 
                  healthy, she'd have quite a few years left in her. Otherwise, 
                  I'm certain it won't be more than a couple of years until she 
                  dies.
                    After that? Perhaps finding the third wife, perhaps 
                  gathering enough wealth to start a legitimate courier 
                  business.
                    Of late, I found myself in Neverwinter, after hearing of an 
                  opportunity for a big score. I figure that if he can make one 
                  more big job, I will be able to head home and give mum the 
                  rest she truly deserves before she dies.
                    I wear, currently, a heavy leather overcoat that functions 
                  as leather armour, sporting a bright purple shirt beneath. I 
                  brandish my rapier on my belt, and when traveling, strap my 
                  short-bow to his back. I keep a neatly trimmed and sculpted 
                  beard, and my wavy, brown hair falls nearly to my shoulders.
                    I've been bestowed the honour of "Cloak Garim Gamwort", 
                  member of the Lord's Alliance, by Sildar Hallwinter.
                    Not liking Nendor much right now, being that he charmed me.
