# Garim Gamwort 
    Chaotic Good Male Lightfoot Halfling
    Level 5 Rogue

## Equipment

### Funds
    PP:
    GP:   217
    EP:    52 
    SP:   387
    CP:
    
### Inventory
    Item                            Location    Quantity    Weight
     Leather Armour                  Worn        1
     Rapier                          Scabbard    1
     Shortbow                        Back        2
     Quiver (19 Arrows)              Back        1
     Daggers                         Boots       2
     Essence of Nightshade           Backpack    1
     Essence of Mercury              Backpack    1
     Essence of Dragon's Bile        Backpack    1
     Thieves Tools                   Backpack    1
     Dragonchess Set                 Backpack    1
     Playing Cards                   Backpack    1
     Crowbar                         Backpack    1
     Rope                            Backpack    1
     bag of 1,000 ball bearings      Backpack    1
     10 feet of string               Backpack    1
     a bell                          Backpack    1
     candles                         Backpack    5
     a hammer                        Backpack    1
     pitons                          Backpack    10
     a hooded lantern                Backpack    1
     flasks of oil                   Backpack    2
     rations                         Backpack    5
     a tinderbox                     Backpack    1
     waterskin                       Backpack    1
     Dark, Hooded clothing           Backpack    1
     moneypouch                      Belt        1
     Horse and cart                  Parked      1
     wolf teeth and claws            Backpack    1
     jade frog w/ gold eyes (40gp)   Backpack    1
     ring (50gp)                     Backpack    1
     potion of healing               Carried     2
     Bag of Holding                  Carried     1

### Bag of Holding
    Item
     Breastplates
     stolen daggers                              2
    
### Details
    
