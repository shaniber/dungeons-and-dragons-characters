# Aengus Grasswalker
    Neutral Good Male Meadowgard Mousefoot
    Level 1 Cleric

## Description
    Name:         Aengus Grasswalker
    Race:         Mousefolk
    Classes:      Cleric 1
    Deity:        Chauntea
    XP:           0
    Alignment:    Neutral Good
    Vision:       Low-light
    Speed:        25ft.
    Languages:    Common, Squeak Speak
    Age:          11        Gender:       male      
    Height:       2'2"      Weight:       20lbs.
    Skin Tone:    n/a       Eyes:         hazel     
    Handedness:   right     Hair:         wavy brown
    Quirks:       
    Speech Style:  
    Quotes:       
    Traits:       Personality   I see omens in every event and action. The 
                                gods try to speak to us, we just need to 
                                listen.
                                
                                Nothing can shake my optimistic attitude.
                  Ideal         Charity. I always try to help those in need, 
                                no matter what the personal cost. (Good) 
                  Bond          Everything I do is for the common people.
                  Flaw          I judge others harshly, and myself even more 
                                severely.
    Background:     I was born in a community on the edge of the Neverwinter 
                  Woods, in sight of the Neverwinter River, at the height of 
                  fall harvest. I was the third of 4 pups in the 4th litter 
                  of my mother, Ingrid. My father, Ashwin, beamed with pride.
                  It was a bountiful harvest that year, and I'm told that 
                  mother didn't need to eat any of our litter!
                    My childhood was relatively easy. At this point, our 
                  colony was large, and we worked hard. That winter, only 
                  two of my littermates died, leaving us a tight-knit family
                  of 12. We all worked hard to make sure that we had food on 
                  the table, but the community was a good generous and kind, 
                  and no family was without. There was never any shortage of 
                  other pups to play with!
                    It was when I was about three years old that I took an 
                  interest in the healing arts. After a giant eagle took 
                  three of my brothers and sisters, and left many others in 
                  the town wounded, I wanted to be able to help in any way.
                  I was introduced to one of the elders, Father Aegis Fleetpaw, 
                  who saw in me a spark. He showed me the tomes of our patron 
                  Goddess, Chauntea, and I knew that I had to follow her light.
                    I spent many seasons learning at Aegis's knee, reading the 
                  texts of Chauntea. I learned of her charity, and gave all 
                  that I had to the temple, so that we could help the less 
                  fortunate amongst us, those who's families had been torn 
                  apart by the eagles, or lost too many family members in the
                  deep chill of winter. I helped direct the planting, and 
                  harvest, of our fields of grain. The lessons I studied lead 
                  me to expect much of myself, and of others.
                    The longer I spent with Father Aegis, however, the more 
                  interest I took in the surrounding world. My curiosity was 
                  nearly insatiable. By the time I had reached adulthood, I 
                  could barely be contained. I took to greeting travellers on 
                  the river, offering to help them with healing, or offering
                  them food as they camped at the edge of the forest. 
                    Father Aegis encouraged me to take the word of Chauntea to
                  Neverwinter. I visited with the farmers along the way, 
                  helping on the farms as I travelled. And as such, I find 
                  myself here, on the outskirts of Neverwinter, looking for 
                  ways to bring her word to those in the city. Word reached my
                  ears of a strange annual event in the town of Oakhurst, 
                  situated in the foothills below Mount Hotenow. Every 
                  midsummer, goblins peaceably show up in the town square and 
                  offer a single piece of magical fruit to the highest bidder 
                  that, when eaten, heals those who suffer from any disease or 
                  ailment. Intrigued by the mystery of how wretched goblins 
                  could ever possess such a wonder , I find myself compelled to 
                  find this rumoured tree of healing so as to use its bounty 
                  to spread Chauntea's mercy.
                
