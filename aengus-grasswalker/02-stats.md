# Aengus Grasswalker
    Neutral Good Male Meadowgard Mousefoot
    Level 1 Cleric

## Stats

### Abilities
         Current     Base   Race  Misc
         Score  Mod  Score  Adj   Adj
    Str:   8     -1    8    --    --
    Dex:  16     +3   14    +2    --
    Con:  13     +1   13    --    --
    Int:  10          10    --    --
    Wis:  16     +3   15    +1    --
    Cha:  12     +1   12    --    --
     

### Skills
    Skill                   Modifier
    Acrobatics (Dex)         +3     
    Animal Handling (Wis)    +3
    Arcana (Int)              0
    Athletics (Str)          -1
    Deception (Cha)          +1
    History (Int)            +2 *
    Insight (Wis)            +5 *
    Intimidation (Cha)       +1
    Investigation (Int)       0
    Medicine (Wis)           +5 *
    Nature (Int)              0
    Perception (Wis)         +5 *
    Performance (Cha)        +1
    Persuasion (Cha)         +1 
    Religion (Int)           +2 *
    Sleight of Hand (Dex)    +3
    Stealth (Dex)            +3
    Survival (Wis)           +3
    ----
        
    

### Proficiencies
    Armour
        Light armor, Medium armor, Shields
    Weapons
        Rapier, Shortsword, Shortbow, Handaxe, Club, Dagger, Greatclub, 
        Javelin, Light Hammer, Mace, Quarterstaff, Sickle, Spear, Unarmed
        Strike.
    Tools
        n/a
     

#### Special
    Brave 
        You have advantage on saving throws against being frightened.
    Keen Senses 
        You have proficiency in the Perception skill.
    Nimble Dodge
        When attacked by an Attack of Opportunity, you may use your reaction 
        to impose disadvantage on that attack.
    Speak With Small Beasts
        Through sounds and gestures, you can communicate simple ideas with 
        Small or smaller beasts.
    Disciple of Life  
        Also starting at 1st level, your healing spells are more effective. 
        Whenever you use a spell of 1st level or higher to restore hit points 
        to a creature, the creature regains additional hit points equal to 
        2 + the spell's level. 
     
#### Feats
    None.
     

#### Misc
    None.
     
