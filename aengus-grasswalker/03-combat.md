# Aengus Grasswalker
    Neutral Good Male Meadowgard Mousefoot
    Level 1 Cleric

## Combat

### Summary
    Hit Die:        1d8 .
    Initiative:     +3
    Speed:          25
    Proficiency:    +2
    Psv Perception: 15
    Inspiration:     -
        

### Defense
    Armour Class:
        Total | Touch | Flat Footed
          16  |  13   |  13
        
    Hit Points
        Total:      9
        ..... ....
        Temp:        0
        -
        
    Death Saves:
        success: ...
        failure: ...
        
    Conditions:
        - 
        
    Effects:
        - 
        
    Saving Throws
        Str:        -1
        Dex:        +3 
        Con:        +2
        Int:         0 
        Wis:        +5 *
        Cha:        +3 *
        

### Offense
    Base Attack
        Melee:      0
        Ranged:     0 
        Grapple:    -1
        
    Mace:
        to hit:     +1
        damage:     1d6-1
        critical:   20/x2
        reach:      5
        
    Shortbow:
        to hit:     +5
        damage:     1d6+3
        critical:   20/x2
        range:      80/320
      
