# Aengus Grasswalker
    Neutral Good Male Meadowgard Mousefoot
    Level 1 Cleric

## Equipment
### Funds
    PP:
    GP:   15
    EP:    0 
    SP:    0
    CP:
    
### Inventory
    Item                            Location    Quantity    Weight
     Holy Symbol                     carried      1
     Leather Armour                  worn         1
     Mace                            right hand   1
     Shield                          equipped     1
     Shortbow                        carried      1
     Quiver of Arrows                carried     20
     Backpack                        worn         1
     Bedroll                         backpack     1
     Mess Kit                        backpack     1
     Tinderbox                       backpack     1
     torches                         backpack    10
     rations                         backpack    10
     waterskin                       carried      1
     50' hempen rope                 backpack     1
     
    
### Details
    
