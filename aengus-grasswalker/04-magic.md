# Aengus Grasswalker
    Neutral Good Male Meadowgard Mousefoot
    Level 1 Cleric

## Magic

### Special
    None
     

### Cleric
    DC:         13
    Attack:     +5
    Cantrips:   3
    Level:      0     1     2     3     4     5     6     7     8     9
    Per Day:    i     2     -     -     -     -     -     -     -     -

#### Prepared Spells
        - 
        - 
        - 
        -


## Spellbook

### Cantrips
#### Guidance
_Divination cantrip_  
**Casting Time:** 1 action  
**Range:** Touch  
**Components:** V, S  
**Duration:** Concentration, up to 1 minute  
**Attack/Save:** none  
You touch one willing creature. Once before the spell ends, the target can 
roll a d4 and add the number rolled to one ability check of its choice. It 
can roll the die before or after making the ability check. The spell then ends.

#### Sacred Flame
_Evocation cantrip_  
**Casting Time:** 1 action  
**Range:** 60 feet  
**Components:** V, S  
**Duration:** Instantaneous  
**Attack/Save:** Dex (DC 13)  
Flame-like radiance descends on a creature that you can see within range. The 
target must succeed on a Dexterity saving throw or take 1d8 radiant damage. 
The target gains no benefit from cover for this saving throw.

The spell’s damage increases by 1d8 when you reach 5th level (2d8), 11th 
level (3d8), and 17th level (4d8).

#### Spare the Dying
_Necromancy cantrip_  
**Casting Time:** 1 action 
**Range:** Touch 
**Components:** V, S 
**Duration:** Instantaneous
**Attack/Save:** none
You touch a living creature that has 0 hit points. The creature becomes 
stable. This spell has no effect on undead or constructs.

### Level 1
