# Ridwan al-Hafayah

## Stats
    Abilities
    --------
             Current     Base   Race  Misc
             Score  Mod  Score  Adj   Adj
        Str:   8     -1    8    --    --
        Dex:  18     +4   16    +2    --
        Con:  16     +3   16    --    --
        Int:  13     +2   13    --    --
        Wis:  10      0   10    --    --
        Cha:  14     +2   13    +1    --
    Skills
    ------
        Skill                   Modifier
        Acrobatics (Dex)          +4
        Animal Handling (Wis)      0
        Arcana (Int)              +1
        Athletics (Str)           -1
      * Deception (Cha)           +4
        History (Int)             +1
      * Insight (Wis)             +2
      * Intimidation (Cha)        +4
        Investigation (Int)       +1
        Medicine (Wis)             0
        Nature (Int)              +1
      * Perception (Wis)          +4
        Performance (Cha)         +2
      * Persuasion (Cha)          +4
        Religion (Int)            +1
        Sleight of Hand (Dex)     +4
      * Stealth (Dex)             +6
        Survival (Wis)             0
    Proficiencies
    -------------
        Armour
            Light armour
        Weapons
            Club, Crossbow (Hand), Crossbow (Light), Dagger, Dart, Greatclub, 
            Handaxe, Javelin, Light Hammer, Longsword, Mace, Quarterstaff, Rapier, 
            Shortbow, Shortsword, Sickle, Sling, Spear, Unarmed Strike
        Tools
            Thieves' Tools, Dragonchess
    Special
    -------

    Misc
    ----
