# Ridwan al-Hafayah
	Chaotic Good Male Genasi Mystic
	Level 2 Mystic

## Description
	Name:			Ridwan al-Hafayah
	Race:       	Genasi (Air)
	Classes:    	Mystic 2
	Experience: 	300 / 900
	Alignment:  	Chaotic Good
	Vision:     	
	Speed:      	Walk 30 ft.
	Languages:  	Common, Primordial
	Age:			32
	Gender:			Male
	Height:			5'11"
	Weight:			169lbs
	Skin Tone:		Dark tan
	Eyes:			Brown
	Handedness:		Right
	Hair:			Dark Brown
	Quirks:			I keep a book with the names of every creatures than I've 
					slain, naming them if they're not known. I also never
					slept in a bed, and have no intention of starting to now.
	Speech Style:
	Quotes:
	Traits:			Personality		I eat like a pig, and have bad table 
									manners.
									I bluntly say what other people are 
									hinting at or hiding. 
					Ideal			Respect. All people, rich or poor, deserve 
									respect. (Good) 
					Bond			I owe my survival to another urchin who 
									taught me to live on the streets. 
					Flaw			People who can't take care of themselves 
									get what they deserve. 
	
